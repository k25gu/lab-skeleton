require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    console.log("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
  ///TODO: Implement this
  //order products table randomly and only get one record
  return (await mysql.query(`SELECT * FROM products ORDER BY RAND() LIMIT 1;`))[0][0];
};

const queryAllProducts = async () => {
  ///TODO: Implement this
  //get all records from products table
  return (await mysql.query(`SELECT * FROM products;`))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

const insertOrder = async (order) => {
  ///TODO: Implement this
  const orderId = uuid.v4();
  //insert the order first, foreign key should already exist in users table
  const smth = await mysql.query(`INSERT INTO orders
                                  VALUES("${orderId}","${order.user_id}","${order.total_amount}");`);

  //populate order_items, since each order can have multiple different products
  for (product of order.products) {
    await mysql.query(`INSERT INTO order_items
                  VALUES("${uuid.v4()}","${orderId}","${product.product_id}","${product.quantity}");`);
  }

  //make sure that the id for the order it returned
  order.id = orderId;

  return order;
};

const updateUser = async (id, updates) => {
  ///TODO: Implement this
  //update user based of user id
  const smth = (await mysql.query(`UPDATE users
                            SET email = "${updates.email}", password = "${updates.password}"
                            WHERE id = "${id}"`));
  //expected return is the id and email of the user updated
  let ret = {id: id, email: updates.email};
  return ret;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
