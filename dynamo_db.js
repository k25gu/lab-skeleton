require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  ///TODO: IMPLEMENT THIS
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  const tableSize = response.Items.length;
  const randomIndex = Math.floor(Math.random() * tableSize);
  return response.Items[randomIndex];
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  ///TODO: Implement this
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  ///TODO: Implement this
  const orderId = uuid.v4();
  //post via key id, and attributes
  const command = new PutCommand({
    TableName: "Orders",
    Item: {
      id: orderId,
      user_id: order.user_id,
      products: order.products,
      total_amount: order.total_amount,
    },
  });

  const response = await docClient.send(command);
  order.id = orderId;
  return order;
};

const updateUser = async (id, updates) => {
  ///TODO: Implement this
  //patch user using key: id, and updating email and password
  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: "set email = :x, password = :y",
    ExpressionAttributeValues: {
      ":x": updates.email,
      ":y": updates.password,
    },
  });

  const response = await docClient.send(command);
  return response;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
